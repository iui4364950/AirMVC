/**Created by the Morn,do not modify.*/
package ui.preload {
	import morn.core.components.*;
	public class PreLoadUI extends View {
		public var prgLoad:ProgressBar;
		protected var uiXML:XML =
			<View>
			  <ProgressBar skin="png.comp.progress" x="18" y="211" width="565" height="14" sizeGrid="2,2,2,2" var="prgLoad" value="0"/>
			</View>;
		public function PreLoadUI(){}
		override protected function createChildren():void {
			createView(uiXML);
		}
	}
}
/**Created by the Morn,do not modify.*/
package ui.login {
	import morn.core.components.*;
	public class LoginUI extends View {
		public var btnLogin:Button;
		protected var uiXML:XML =
			<View>
			  <Image url="png.comp.bg" x="128" y="103" sizeGrid="4,26,4,4" width="354" height="206"/>
			  <Button label="登录" skin="png.comp.button" x="271" y="240" width="75" height="28" var="btnLogin"/>
			  <Label text="Welcome to AirMVC demo" x="155" y="186" color="0x3333ff" bold="true" size="24"/>
			</View>;
		public function LoginUI(){}
		override protected function createChildren():void {
			createView(uiXML);
		}
	}
}
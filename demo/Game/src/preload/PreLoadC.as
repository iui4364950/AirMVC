package preload 
{
	import global.Nws;
	import morn.core.handlers.Handler;
	import org.airmvc.Controller;
	import org.airmvc.ModulesManager;
	
	/**
	 * ...
	 * @author WLDragon
	 */
	public class PreLoadC extends Controller 
	{
		
		public function PreLoadC() 
		{
			super();
		}
		
		override protected function addListeners():void 
		{
			
		}
		
		override protected function startup(... args):void 
		{
			//加载进度条必需资源
			App.loader.loadAssets(["assets/ui/comp.swf"], new Handler(onComplete));
		}
		
		private function onComplete():void 
		{
			if (!view)
			{
				registerView(new PreLoadV());
				send(SHOW_OR_HIDE);
				//加载游戏配置数据...
				
				//加载初始化资源...
				
				//解析数据和资源...
				
				App.timer.doLoop(100, onFrame);
				
			}
		}
		
		/**
		 * 模拟进度条
		 */
		private function onFrame():void 
		{
			if (progress < 1)
			{
				progress += .05;
				send(SET_PROGRESS,progress);
			}
			else
			{
				broadcast(Nws.STARTUP_LOGIN,Nws.STARTUP_LOGIN);
				send(DESTROY);
				view = null;
				ModulesManager.remove(Nws.STARTUP_PRELOAD);
				App.timer.clearTimer(onFrame);
			}
		}
		
		private var progress:Number = 0;
		
		static public const SET_PROGRESS:String = "set_progress";
		static public const DESTROY:String = "destroy";
		static public const SHOW_OR_HIDE:String = "show_or_hide";
	}

}
package scene.views 
{
	import flash.events.MouseEvent;
	import org.airmvc.Dispatcher;
	import scene.SceneC;
	import ui.scene.SceneUI;
	
	/**
	 * ...
	 * @author WLDragon
	 */
	public class Scene extends SceneUI 
	{
		
		public function Scene() 
		{
			super();
			
			btnBack.addEventListener(MouseEvent.CLICK,onClick);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			//ViewComponent通过Dispatcher的send方法发送消息，只有SceneC或SceneV才能接收
			Dispatcher.send(SceneC.OPEN_LOGIN,this);
		}
		
	}

}
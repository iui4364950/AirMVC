package login 
{
	import global.Nws;
	import org.airmvc.Controller;
	
	/**
	 * ...
	 * @author WLDragon
	 */
	public class LoginC extends Controller 
	{
		
		public function LoginC() 
		{
			super();
			
		}
		
		override protected function addListeners():void 
		{
			receive(ENTRY_SCENE,handleEntryScene);
		}
		
		override protected function startup(... args):void 
		{
			if (view)
			{
				send(SHOW_OR_HIDE);
			}
			else
			{
				registerView(new LoginV());
				send(SHOW_OR_HIDE);
			}
			
		}
		
		private function handleEntryScene():void 
		{
			broadcast(Nws.STARTUP_SCENE,Nws.STARTUP_SCENE);
			send(SHOW_OR_HIDE);
		}
		
		static public const SHOW_OR_HIDE:String = "show_or_hide";
		static public const ENTRY_SCENE:String = "entry_scene";
	}

}
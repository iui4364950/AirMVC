package  
{
	import login.LoginC;
	import preload.PreLoadC;
	import scene.SceneC;
	/**
	 * 模块控制类定义配置文件
	 * @author WLDragon 2014-02-06
	 */
	public class ModulesConfig 
	{
		public function ModulesConfig()
		{
			firstModule = "preload.PreLoadC";
			modules = [
				{def:"preload.PreLoadC",url:PreLoadC},
				{def:"login.LoginC", url:LoginC },
				{def:"scene.SceneC",url:SceneC}
			];
		}
		public var firstModule:String;
		public var modules:Array;
	}

}
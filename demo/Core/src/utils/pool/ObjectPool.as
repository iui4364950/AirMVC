package utils.pool 
{
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	/**
	 * 对象池管理类
	 * @author WLDragon 2013-12-20
	 */
	public class ObjectPool 
	{
		
		public function ObjectPool() 
		{
			init();
		}
		
		private function init():void 
		{
			pools = new Dictionary;
		}
		
		/**
		 * 创建对象池
		 * @param	$class 对象类型
		 * @param	$size 池大小
		 */
		public function createPool($class:Class,$size:int):void
		{
			if (pools[$class] == undefined)
			{
				var p:Pool = new Pool;
				p.size = $size;
				p.belly = [];
				for (var i:int; i < $size;i++ )
				{
					p.belly.push(new $class());
				}
				pools[$class] = p;
			}
			else
			{
				trace(getQualifiedClassName($class) + "池已经被创建");
			}
		}
		
		/**
		 * 从对象池中取出对象
		 * @param	$class 对象类型
		 * @param   $arg   重新初始化需要的参数
		 * @return 对象实例
		 */
		public function take($class:Class,$arg:* = null):IPoolable
		{
			var o:IPoolable;
			var p:Pool = pools[$class];
			
			if (p.belly.length > 0)
			{
				o = p.belly.pop();
			}
			else
			{
				//如果池已经空了则实例新对象再返回
				trace("3:##需要添加新的" + getQualifiedClassName($class) + "对象,用于统计,如果出现数量比较多则池应该再建大点");
				o = new $class();
			}
			
			o.renew($arg);
			
			return o;
		}
		
		/**
		 * 归还对象到对象池中
		 * @param	$obj 对象
		 * @param   $class 类型
		 */
		public function giveBack($obj:IPoolable,$class:Class):void
		{
			$obj.release();
			
			var p:Pool = pools[$class];
			if (p.belly.length < p.size)
			{	
				p.belly.push($obj);
			}
			else
			{
				//如果池满了就不回收了
				$obj = null;
			}
		}
		
		/**池林*/
		private var pools:Dictionary;
	}

}

class Pool
{
	/**池的胃(内容)*/
	public var belly:Array;
	/**池的容量*/
	public var size:int;
}
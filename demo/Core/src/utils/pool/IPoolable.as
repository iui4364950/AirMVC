package utils.pool 
{
	
	/**
	 * 可放在对象池的对象接口
	 * @author WLDragon
	 */
	public interface IPoolable 
	{
		/**
		 * 从池中获取后重新初始化
		 * @param	arg 需要传入的参数
		 */
		function renew(arg:Object = null):void;
		/**
		 * 回收到池之前做部分内存释放工作
		 */
		function release():void;
	}
	
}
﻿package vo
{
	/**
	 * 玩家表
	 * @author WLDragon 2013-11-10
	 */
	public class PlayerVO
	{
		/**玩家标识*/
		public var id:int;
		/**君主（General）*/
		public var monarchID:int;
		/**军令数量*/
		public var order:int;
		/**玩家势力的颜色*/
		public var color:uint;
	}
}
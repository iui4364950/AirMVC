package org.airmvc 
{
	import flash.display.DisplayObjectContainer;
	import flash.utils.Dictionary;
	/**
	 * 控制器
	 * @author WLDragon
	 */
	public class Controller
	{
		
		public function Controller() 
		{
			messageMap = new Dictionary();
		}
		
		/**
		 * 加载模块完成后初始化
		 * @param	definition 模块控制类定义
		 * @param	args       参数
		 */
		internal function start(definition:String,args:Array):void
		{
			ModulesManager.dispatch.interest(definition, startup);
			addListeners();
			startup.apply(null,args);
		}
		
		/**
		 * 当模块收到启动消息的时候执行此函数
		 * 该方法的第一个参数应该是模块名，向后为自定义参数
		 * 第一次发送“modules.begin.BeginC”消息时会加载Begin模块然后执行BeginC的startup，当再次发送该消息时不重复加载只执行BeginC里的startup函数
		 * 必须在Controller的子类重写startup方法
		 * 在此方法中处理视图的初始化以及打开关闭视图
		 */
		protected function startup(... args):void { throw new Error("请重写startup方法!"); };
		
		/**
		 * 添加消息监听
		 */
		protected function addListeners():void { throw new Error("请重写addListeners方法!");  };
		
		
		/**
		 * 向其他模块广播消息，自身也可以关注广播的消息
		 * @param	news     消息名
		 * @param	... args 参数
		 */
		protected function broadcast(news:String, ... args):void
		{
			ModulesManager.dispatch.broadcast(news,args);
		}
		
		/**
		 * 关注广播的消息
		 * @param	news    消息名
		 * @param	handler 处理函数
		 */
		protected function interest(news:String, handler:Function):void
		{
			ModulesManager.dispatch.interest(news,handler);
		}
		
		/**
		 * 取消关注
		 * @param	news    消息名
		 * @param	handler 处理函数
		 */
		protected function cancelInterest(news:String, handler:Function):void
		{
			ModulesManager.dispatch.cancelInterest(news,handler);
		}
		
		/**
		 * 注册视图管理器
		 * @param	view      视图管理器
		 */
		protected final function registerView(view:View):void
		{
			this.view = view;
			this.view.bindMessageMap(messageMap);
		}
		
		/**
		 * 模块内部向视图管理器发送消息
		 * @param	msg      消息名
		 * @param	... args 参数
		 */
		protected function send(msg:String, ... args):void
		{
			(messageMap[msg] as Function).apply(null,args);
		}
		
		/**
		 * 接收模块内部视图管理器的消息
		 * @param	msg    消息名
		 * @param	handle 处理函数
		 */
		protected function receive(msg:String,handle:Function):void
		{
			messageMap[msg] = handle;
		}
		
		/**
		 * 取消对消息的接收
		 * @param	msg 消息名
		 */
		protected function cancelReceive(msg:String):void
		{
			messageMap[msg] = null;
			delete messageMap[msg];
		}
		
		/**消息地图*/
		private var messageMap:Dictionary;
		/**视图管理类*/
		protected var view:View;
	}

}